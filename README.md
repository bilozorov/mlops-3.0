# MLOps 3.0

## ML Lifecycle Process

We are starting this project using a methodology of [CRISP-ML(Q)](https://ml-ops.org/content/crisp-ml)

## How to work in repository (Data Science Lifecycle Process)
- The **main branch** for production code
- The **dev branch** for preproduction, experiments.
- Each participant of the project creates a separate **feature branch** for his own experiments. Successful results are sent to the **dev branch**.

## Environments
The mamba package manager is used in the project. 
The following environments exist:
- mlops_prod – default environment with dependencies fixed in the environment_prod.yml file
- mlops – delopment environment with linker and formatter. Dependencies are fixed in the environment.yml file

## How to create and run Docker image
1. Clone the repository:  
```git clone https://gitlab.com/bilozorov/mlops-3.0.git```
2. Build the image with production dependencies .  
```docker build . -t <image_name>```
3. Run the container.  
```docker run --name <container_name> --rm -u 0 -v <local_path>:/app <image_name>```
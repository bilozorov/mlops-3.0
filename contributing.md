## Contributing - Using Ruff Linters

This project utilizes **Ruff** as the primary linter to ensure code quality and consistency. Here's a guide for contributors:

**Understanding Ruff**

* **Ruff** is a powerful linter and formatter for Python code.
* It enforces code style, identifies potential errors, and suggests improvements.

**Setting Up Ruff:**

1. **Install Python:** Ensure you have Python 3.8 or above installed.
2. **Install Ruff:** Use pip to install Ruff:

```bash
pip install ruff
```

**Running Ruff**

* Navigate to your project's root directory in your terminal.
* Execute the following command to initiate linting:

```bash
ruff check
```

This will scan your project and report any issues discovered by Ruff.

Or you can check one file:

```bash
ruff check <path>
```

**Pre-commit Integration (Optional):**

* Consider integrating Ruff with pre-commit to automatically run checks before committing changes.
* Add the following configuration to your `.pre-commit-config.yaml` file:

```yaml
repos:
- repo: https://github.com/astral-sh/ruff-pre-commit
  rev: v0.3.5  # Adjust version as needed
  hooks:
  - id: ruff
    args: ["--check"]
  - id: ruff-format
```

This will run `ruff check` before every commit, ensuring code adheres to linting standards.

**Additional Tips:**

* Explore the Ruff documentation for a detailed list of rules and configuration options: [https://github.com/astral-sh/ruff](https://github.com/astral-sh/ruff)
* Consider using `ruff format` to automatically format your code according to Ruff's style guidelines.

**Benefits of Using Ruff:**

* Improves code readability and maintainability.
* Catches potential errors early in the development process.
* Enforces consistent coding style across the project.
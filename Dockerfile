FROM mambaorg/micromamba

WORKDIR /app

COPY env.yml /app/env.yml
RUN micromamba create -f env.yml
COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock
RUN micromamba run -n mlops poetry install --with dev

# ENTRYPOINT ["micromamba", "run", "-n", "mlops", "jupyter", "notebook", "--ip=0.0.0.0",  "--port=8888", "--allow-root" ]
